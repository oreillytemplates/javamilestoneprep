package com.oreillyauto;

public class StaticExample {

    public StaticExample() {
    }

    private static String getCommaSeparatedString(int[] intArray) {
        StringBuilder sb = new StringBuilder();
        
        for (int i : intArray) {
            sb.append((sb.toString().length() > 0) ? (", " + i) : i);
        }
        
        return sb.toString();
    }
    
    public static void main(String[] args) {
        //System.out.println(new StaticExample().getCommaSeparatedString(new int[] {1,2,3}));
        System.out.println(StaticExample.getCommaSeparatedString(new int[] {1,2,3}));
    }
}
