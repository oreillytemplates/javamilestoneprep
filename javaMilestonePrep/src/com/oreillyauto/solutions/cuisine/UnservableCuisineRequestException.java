package com.oreillyauto.solutions.cuisine;

class UnservableCuisineRequestException extends Exception {
    private static final long serialVersionUID = 7061797303024953619L;

    public UnservableCuisineRequestException(String message) {
        super(message);
    }
}