package com.oreillyauto.solutions.cuisine;
abstract class Cuisine {
    public abstract Cuisine serveFood(String dish);
}