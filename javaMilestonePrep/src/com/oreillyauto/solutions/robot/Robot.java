package com.oreillyauto.solutions.robot;
class Robot {
    Integer currentX=0;
    Integer currentY=5;
    Integer previousX=0;
    Integer previousY=5;
    String lastMove = "";
    int xDiff = 0;
    int yDiff = 0;
    
    public Robot() {    
    }
     
    public Robot(Integer x, Integer y) {
        this.currentX = x;
        this.currentY = y;
        this.previousX = x;
        this.previousY = y;
    }
     
    public void moveX(Integer dx) {
        // Move the robot from current position (x, y)
        // to new position (x + dx, y). 
        // Remember to maintain previousX.
        previousX = new Integer(currentX);
        currentX += dx;
        lastMove = "x";
        xDiff = currentX - previousX;
        previousY = currentY;
    }
     
    public void moveY(Integer dy) {
        // Move the robot from current position (x, y)
        // to new position (x, y + dy). 
        // Remember to maintain previousY.
        previousY = new Integer(currentY);
        currentY += dy;
        lastMove = "y";
        yDiff = currentY - previousY;
        previousX = currentX;
    }
     
    public String printCurrentCoordinates() {
        // Print two space-separated integers describing
        // the robots current x and y coordinates
        //System.out.println(currentX + " " + currentY);
        return currentX + " " + currentY;
    }
    
    public String printLastCoordinates() {
        // Print two space-separated integers describing
        // the robots previous x and y coordinates
        //System.out.println(previousX + " " + previousY);
        return previousX + " " + previousY;
    }
    
    public String printLastMove() {
        // Print two space-separated values describing
        // the robots most recent movement.
        // example 1: if last move was moveX(dx) print "x dx" 
        //            where x is the literal character x and 
        //            dx is the distance moved in x direction
        //            during the last call to moveX.
        // example 2: if last move was moveY(dy) print "y dy" 
        //            where y is the literal character y and 
        //            dy is the distance moved in y direction
        //            during the last call to moveY.
        StringBuilder sb = new StringBuilder();
        sb.append(lastMove + " ");
        sb.append(("x".equalsIgnoreCase(lastMove)) ? xDiff : yDiff);
        return sb.toString();
    }
    
 }