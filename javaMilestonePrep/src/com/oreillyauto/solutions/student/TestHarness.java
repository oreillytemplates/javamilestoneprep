package com.oreillyauto.solutions.student;

import java.util.Scanner;

public class TestHarness {   
    private static final Scanner INPUT_READER = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOfStudents = Integer.parseInt(INPUT_READER.nextLine());
        
        while (numberOfStudents-- > 0) {
            String studentName = INPUT_READER.nextLine();
            Student student = new Student(studentName);
            
            System.out.println(student.toString());
        }
    }
}