package com.oreillyauto.solutions.carinheritance;
class HondaCity extends Car {
    int mileage;
    public final static boolean IS_SEDAN = true;
    public final static String SEAT_COUNT = "4";
    
    public HondaCity(int mileage) {
        super(IS_SEDAN, SEAT_COUNT);
        this.mileage = mileage;
    }
    
    public String getMileage() {
        return mileage + " kmpl" ;
    } 
}