package com.oreillyauto.solutions.compusa;

public class LaptopTransaction {

	public static String addLaptop(Laptop laptop, int count) throws TransactionException{
		if (count<1){
			throw new TransactionException("Count should be greater than zero.", "INVALID_COUNT");
		} else if(laptop.getSerialNumber()==null) {
			throw new TransactionException("Serial Number Missing.", "SERIAL_NUMBER_MISSING");
		} else{
			Laptop.setLaptopCount(Laptop.getLaptopCount() + count);
			//System.out.println("Laptops successfully added.");
			return "Laptops successfully added.";
		}
	}
	
	public static String sellLaptop(Laptop laptop, int count) throws TransactionException {
        if (laptop.getSerialNumber() == null) {
            throw new TransactionException("Serial Number Missing.", "SERIAL_NUMBER_MISSING");
        } else if (Laptop.getLaptopCount() < count) {
            throw new TransactionException("Out of stock.", "OUT_OF_STOCK");
        } else if (count < 1) {
            throw new TransactionException("Count should be greater than zero.", "INVALID_COUNT");
        } else {
            Laptop.setLaptopCount(Laptop.getLaptopCount() - count);
            //System.out.println("Laptops successfully sold.");
            return "Laptops successfully sold.";
        }
	}
}
