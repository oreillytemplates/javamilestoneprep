package com.oreillyauto.solutions.compusa;

public class Laptop {
	private String SKU;
	private String serialNumber;
	private static int laptopCount=0;
	
	public Laptop(String SKU, String serialNumber) {
		this.SKU=SKU;
		this.serialNumber=serialNumber;
	}

	public String getSKU() {
		return SKU;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public static int getLaptopCount() {
		
		return laptopCount;
	}

	public void setSKU(String sKU) {
		SKU = sKU;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public static void setLaptopCount(int laptopCount) {
		Laptop.laptopCount = laptopCount;
	}
}
