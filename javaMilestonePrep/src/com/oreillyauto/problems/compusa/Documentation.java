package com.oreillyauto.problems.compusa;

public class Documentation {
    /**
     * You are the Operations Manager for CompUSA.
     * You are responsible for managing inventory.
     * Two new laptops are coming to your store and you want to monitor the laptop transactions.
     * 
     * The laptop transactions are validated before being sold. 
     * 
     * For an invalid transactions the following error codes and the error messages are displayed:
     *   "SERIAL_NUMBER_MISSING" This errorCode reports the unauthorized transaction. If there is 
     *                           no Serial Number for the laptop. The error message should be:
     *                                    "Serial number missing"
     * 
     *   "OUT_OF_STOCK"          This errorCode reports the attempt of selling a laptop that is not 
     *                           in stock. The errorMessage should be:
     *                                    "Out of stock"
     * 
     *   "INVALID_COUNT"         This errorCode reports selling an invalid number of laptops (e.g. 0). 
     *                           The errorMessage should be:
     *                                    "Count should be greater than zero"
     * 
     * You are responsible for building a laptop transaction workflow by writing the complete 
     * implementation of the following three classes:
     * 
     * The "TransactionException.java" class should implement:
     *  - The constructor TransactionException(String errorMessage, String errorCode).
     *  - The method String getErrorCode() to return the error code of the exception being thrown.
     *  - The method String getMessage() to return the message of the exception being thrown.
     *  - The class should extend Exception.
     * 
     * The "Laptop.java" class should implement:
     *  - The constructor Laptop(String SKU, String serialNumber).
     *  - The method String getSerialNumber() to return the serial number.
     *  - The method String getSKU() to return the SKU.
     *  - The method int getLaptopCount()
     *  - The method setLaptopCount()
     * 
     * The LaptopTransaction class should implement:
     *  - The method: String addLaptop(Laptop laptop, int count) 
     *    Adds laptops to the inventory. It should also throw the required exceptions for any 
     *    invalid transactions. If successful, the method should return:
     *       "Laptops successfully added."
     *    
     *  - The method: String sellLaptop(Laptop laptop, int count) to decrement the laptop count 
     *    from the inventory. It should also throw the required exceptions for any invalid 
     *    transactions. If successful, the method should return:
     *       "Laptops successfully sold."
     * 
     * Build a test harness to run transactions.
     * Force the errors to be thrown.
     * Program Execution continues regardless of error.
     * Program 3 Inputs and Outputs
     * 
     * INPUT:
     * 2 : types of laptops
     * 1 Dell m6800
     * 2 HP
     * 10: transactions
     * 1 sell 50
     * 1 add 100
     * 1 add 0
     * 1 sell 30
     * 3 add 500
     * 1 add -5
     * 1 add 1000
     * 1 sell -20
     * 1 sell 100
     * 1 add 720
             * 
     * EXPECTED OUTPUT:
     * OUT_OF_STOCK: Out of stock.
     * Laptops successfully added.
     * INVALID_COUNT: Count should be greater than zero.
     * Laptops successfully sold.
     * SERIAL_NUMBER_MISSING: Serial Number Missing.
     * INVALID_COUNT: Count should be greater than zero.
     * Laptops successfully added.
     * INVALID_COUNT: Count should be greater than zero.
     * Laptops successfully sold.
     * Laptops successfully added.
     * 
     * EXPLANATION:
     * For each of the invalid transactions for Dell m6800 (1), required exceptions 
     * are thrown. Otherwise, the transactions occur.
     * When a 3 is passed in (laptop type), it is an invalid (Line 5 of Expected Output). 
     * 
     * Create necessary classes, uncomment the code in the TestHarness, and
     * then run TestHarness.java to see the results. 
     * 
     */
    
}