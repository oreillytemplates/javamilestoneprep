package com.oreillyauto.problems.geometry;

public interface Triangle {
    public int area(int base, int height); 
}